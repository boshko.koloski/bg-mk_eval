from numpy.random.mtrand import random
import pandas as pd
import numpy as np
import tensorflow as tf
from transformers import AutoTokenizer
from datasets import load_dataset, load_metric
from transformers import TrainingArguments
from transformers import Trainer
from transformers import AutoModelForSequenceClassification
from transformers import TrainingArguments
from datasets import load_metric
from datasets.features import ClassLabel, Sequence

import os
import time
model_name = "../bg_mk/scratch_100000"# "yiyanghkust/finbert-esg" #"yiyanghkust/finbert-pretrain"#, "michiyasunaga/LinkBERT-base"

def preprocess_function(examples):
    tokenizer = AutoTokenizer.from_pretrained(model_name, local_files_only = False)
    tokens = tokenizer(examples["text_a"],truncation=True,padding=True, add_special_tokens = True)
    return tokens
seeds = [1903,1991,31,773,1,2016,83,513,213,808]

# Data
#dataset = "bbc"
i = 0

for dataset_name in ['hw_cf']:        
    dataset_prefix = f"{dataset_name}"
    data_files = {"train": "train_hw.csv", "dev": "dev_hw.csv", "test": "test_hw.csv"}
    output = []

    start = time.time()

    dataset = load_dataset(dataset_prefix, data_files=data_files, delimiter=",", download_mode='force_redownload')
    print(dataset['train'].features)
    tokenized_dataset = dataset.map(preprocess_function, batched=True)
    print(tokenized_dataset['train'][0])
    num_labels = len(set(pd.read_csv(dataset_prefix+"/train_hw.csv", delimiter=",")['label']))
    
    tokenizer = AutoTokenizer.from_pretrained(model_name, local_files_only = False)

    model = AutoModelForSequenceClassification.from_pretrained(model_name, num_labels=num_labels)# problem_type="multi_label_classification")

    training_args = TrainingArguments(
        output_dir='./results',
        per_device_train_batch_size=1,
        num_train_epochs=1,
        save_total_limit=0,
        save_strategy="no",
        seed=seeds[i]
    )
    # Trainer object initialization
    trainer = Trainer(
        model=model,
        args=training_args,
        train_dataset=tokenized_dataset['train'],
        eval_dataset=tokenized_dataset['dev'],
        tokenizer=tokenizer,
        #data_collator=data_collator,
    )

    trainer.train()

    trainer.save_model()

    stop = time.time()
    print(f"Training time: {stop - start}s")
    metric_acc = load_metric("accuracy")
    metric_f1 = load_metric("f1")

    for split in ['train','dev','test']:
        scores = {}
        scores['split']  = split
        split_lbls = tokenized_dataset[split]['label']
        pred = trainer.predict(tokenized_dataset[split])
        model_predictions = np.argmax(pred[0], axis=1)
        scores['accracy'] = metric_acc.compute(predictions=model_predictions, references=split_lbls)
        
        for metric in ['macro','micro','weighted']:
            score = metric_f1.compute(predictions=model_predictions, references=split_lbls, average=metric)
            scores[metric] = score
        output.append(scores)

    out_df = pd.DataFrame(output)
    out_df['time'] = [stop-start]*3
    out_path = f"./results/{model_name}/{dataset_name}/"
    os.makedirs(out_path, exist_ok=True)
    out_df.to_csv(f'{out_path}/{str(stop)}.csv',index=None)