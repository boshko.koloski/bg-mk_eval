import pandas as pd
import json
linez = []
with open("data/it_mk_txts.json") as f:
    for l in f:
        line = json.loads(l)
        linez.append(line)
out_df = pd.DataFrame(linez)

out_df = out_df.dropna()
out_df = out_df#.sample(5000)
print(out_df.columns)#['text'].to_list()[0])

texts = []
lbls = []
lbls_raw = []
for t in out_df.text:
    parts = t.split('\n\n')
    max_p = ""
    for p in parts:
        if len(p) > len(max_p):
            max_p = p
    max_p = max_p.split('  ')
    final_p = ""
    for p in max_p:
        if len(p) > len(final_p):
            final_p = p

    lbls_raw.append(max_p[0])
    try:
        lbls.append(";".join([c.strip() for c in max_p[0].split('во')[1].split(' ') if len(c) > 0]))
    except:
        lbls.append("")
    texts.append(final_p)
new_df = pd.DataFrame()
new_df['keywords'] = out_df['tags']
new_df['raw_l'] = lbls_raw
new_df['title'] = [""]  * len(texts)
new_df['abstract'] = texts
#new_df['lbls'] = lbls
new_df['language'] = ["mkd"] * len(texts)
new_df.to_csv('it_mk_prep.csv')
print(new_df.dropna())
train_df = new_df.sample(4000)
test_df = new_df.drop(train_df.index)
